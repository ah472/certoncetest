This is a small web application to show basic routing in react, as well as to test functionality of the blockcerts APIs that are provided.

There are two main components of interest: Test.js and Verify.js

Verify.js uses the blockcerts-verifier library, which uses a completed component and the verification API in order to verify a certificate
Test.js uses the cert-verifier-js library, and this is a work in progress where we try to get verification of a certificate
under the hood more than using a fully build library.

It makes sense to have Verify as a template for what we want to achieve (and look into this library) so we can understand how
cert-verifier-js is being used.