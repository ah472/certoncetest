import React, { Component } from 'react';
import '@blockcerts/blockcerts-verifier';


class Verify extends Component {

  constructor (props) {
    super (props);
    this.state = {
      certificateDirectory: "../artifacts/certificate.json",
      certificate: null,
    }
  }

  render() {
    return(
      <blockcerts-verifier></blockcerts-verifier>
    );
  }

}

export default Verify